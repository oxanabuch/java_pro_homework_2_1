package com.company;

public class Main {

    public static void main(String[] args) {

        int[] array = {77, 145};
        int arithmeticMean = 0;

        for (int i : array) {
            arithmeticMean += i;
        }
        System.out.println("ArithmeticMean: " + arithmeticMean / 2);
    }
}
